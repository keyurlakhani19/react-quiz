import React, { useEffect } from 'react';
import { Routes, Route, useNavigate } from 'react-router-dom';

const QuizMakerRoutes = React.lazy(() => import("./quiz-maker/QuizMakerRoutes"));

function App() {
  const navigate = useNavigate();
  useEffect(() => {
    navigate('/quiz-maker')
  }, [])
  return (
    <Routes>
      <Route path='/quiz-maker/*' element={<React.Suspense fallback={<>...</>}>
          <QuizMakerRoutes />
        </React.Suspense>} />
    </Routes>
  );
}

export default App;
