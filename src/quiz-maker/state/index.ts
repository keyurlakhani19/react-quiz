import { createContext, useContext, useReducer } from "react";

import { Action, QuizMakerAppState } from "../model";
import reducers, { initialState } from "../reducers";

export const StateContext = createContext<QuizMakerAppState | null>(null);
export const StateDispatchContext = createContext<React.Dispatch<Action> | null>(null);

export const useStateReducer = () => useReducer(reducers, initialState);

export const useStateContext = () => useContext(StateContext);
export const useStateDispatchContext = () => useContext(StateDispatchContext) as React.Dispatch<Action>

export const useQuestionStateContext = () => (useStateContext() as QuizMakerAppState).question;
export const useCategoryStateContext = () => (useStateContext() as QuizMakerAppState).category;
export const useAnswerStateContext = () => (useStateContext() as QuizMakerAppState).answer;
export const useFilterStateContext = () => (useStateContext() as QuizMakerAppState).filter;
