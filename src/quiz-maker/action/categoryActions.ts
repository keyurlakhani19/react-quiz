import { fetchCategories } from "../api"
import { Category } from "../model";
import { ActionType } from "../reducers/categoryReducer";

export const loadCategoriesAction = () => ({type: ActionType.Load}) 
export const loadCategoriesSuccessAction = (payload: Category[]) => ({type: ActionType.LoadSuccess, payload})

export const loadCategories = async (dispatch: React.Dispatch<{type: string}>) => {
  const payload = await fetchCategories();
  dispatch(loadCategoriesSuccessAction(payload))
}