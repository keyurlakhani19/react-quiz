import { ActionType } from "../reducers/answerReducer";

export const selectAnswerAction = (payload: {}) => 
    ({type: ActionType.SelectAnswer, payload})

export const submitAnswerAction = (payload: unknown) => 
    ({type: ActionType.Submit, payload}) 

export const resetAnswerAction = () => 
    ({type: ActionType.Reset, payload: null}) 