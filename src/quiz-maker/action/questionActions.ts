import { fetchQuestions } from "../api"
import { FetchQuestionsOptions, FilterStateValue, Question } from "../model";
import { ActionType } from "../reducers/questionReducer";


export const resetQuestionAction = () => ({type: ActionType.Reset})
export const loadQuestionsAction = (...args: unknown[]) => {
  console.log('...args', args);
  return ({type: ActionType.Load})
}
export const loadQuestionsSuccessAction = (payload: Question[]) => ({type: ActionType.LoadSuccess, payload})

export const loadQuestions = async (dispatch: React.Dispatch<{type: string}>, options: FetchQuestionsOptions) => {
  try {
    dispatch(loadQuestionsAction());
    const payload = await fetchQuestions(options);
    dispatch(loadQuestionsSuccessAction(payload))
  } catch (error) {
    //
  }
  
}