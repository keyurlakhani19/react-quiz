import { FilterState } from "../model"
import { ActionType } from "../reducers/filterReducer"


export const submitFilterAction = (payload: FilterState) => ({type: ActionType.Submit, payload}) 
export const resetFilterAction = () => ({type: ActionType.Reset}) 