import React from 'react';

import { 
  StateContext, StateDispatchContext, 
  useStateReducer 
} from "./state";

export const StateProvider = ({ children }: { children: React.JSX.Element }) => {
  const [state, dispatch] = useStateReducer();
  return (
    <StateContext.Provider value={state}>
      <StateDispatchContext.Provider value={dispatch}>
        {children}
      </StateDispatchContext.Provider>
    </StateContext.Provider>
  )
}
