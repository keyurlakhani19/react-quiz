import { StateProvider } from "./StateProvider"
import { QuizMaker } from "./components/QuizMaker"
import {Outlet} from 'react-router-dom'

export const QuizMakerApp = () => {
  return (
    <StateProvider>
      <Outlet />
    </StateProvider>
  )
}