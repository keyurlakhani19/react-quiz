
export interface Category {
  id: number;
  name: string;
}

export interface CategoryState {
  loading: boolean;
  data: Category[];
  error: string;
}