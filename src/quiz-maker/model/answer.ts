
export interface Answer {
  answer: string;
  answerIndex: number;
  correctAnswer: string;
  questionIndex?: number
}

export interface AnswerState {
  data: Answer[];
  score: number;
  submitted: boolean;
}
