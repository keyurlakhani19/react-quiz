export interface Question {
  category: string;
  type: string;
  difficulty: string;
  question: string;
  correct_answer: string;
  incorrect_answers: string[]
  all_answers: string[]
}

export interface QuestionState {
  loading: boolean;
  data: Question[];
  error: string;
}

export interface FetchQuestionsOptions {
  categoryId: number;
  difficulty: string;
  amount?: number;
  type?: string
}
