import { AnswerState } from './answer';
import { Category, CategoryState } from './category';
import { QuestionState } from './question';

export * from './category'
export * from './question'
export * from './answer'

export interface QuizMakerAppState {
  category: CategoryState;
  question: QuestionState,
  answer: AnswerState,
  filter: FilterState
}

export interface Action<T = unknown> {
  type: string;
  payload?: T;
}

export interface SelectOption<T = unknown> {
  value: T;
  viewValue: string;
}

export interface FilterStateValue {
  category: Category | null;
  difficulty: string;
}

export interface FilterState {
  value: FilterStateValue;
  invalid: boolean
}