import { Category, Question, FetchQuestionsOptions } from "./model";
import { ActionType as CategoryActionType } from "./reducers/categoryReducer";

const API_URL = 'https://opentdb.com'

export const fetchCategories = async () => {
  const resp = await fetch(`${API_URL}/api_category.php`);
  const data: { trivia_categories: Category[] } = await resp.json();
  return data.trivia_categories;
}

export const fetchQuestions = async (options: FetchQuestionsOptions) => {
  const { amount = 5, categoryId, difficulty, type = 'multiple' } = options;
  const resp = await fetch(
    `${API_URL}/api.php?amount=${amount}&category=${categoryId}&difficulty=${difficulty.toLowerCase()}&type=${type}`
  );
  const { results: questions }: { results: Question[] } = await resp.json();

  return questions.map(question => {
    return {
      ...question,
      all_answers: [
        ...question.incorrect_answers,
        question.correct_answer]
        .sort(() => (Math.random() > 0.5) ? 1 : -1)
    }
  });
}

