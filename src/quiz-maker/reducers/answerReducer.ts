import { Action, Answer, AnswerState } from "../model";

export const answerState: AnswerState = {
  data: [],
  score: 0,
  submitted: false
}

export const ActionType = {
  SelectAnswer: '[Answer] Select',
  SetScore: '[Answer] Set Score',
  Submit: '[Answer] Submit',
  Reset: '[Answer] Reset'
}

export const answerReducer = (state = answerState, action: Action<unknown>) => {
  switch (action.type) {
    case ActionType.SelectAnswer:
      const data = state.data;
      data[(action.payload as Answer).questionIndex as number] = action.payload as Answer;
      return {
        ...state,
        data
      }

    case ActionType.Submit:
      let score = 0;
      state.data.forEach(item => {
        if(item.answer === item.correctAnswer) {
          score++;
        }
      })
      return {
        ...state,
        score,
        submitted: action.payload as boolean
      }

    case ActionType.Reset:
      return {
        data: [],
        score: 0,
        submitted: false
      }
  
    default:
      return state
  }
}