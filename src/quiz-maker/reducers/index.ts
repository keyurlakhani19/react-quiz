import { Action, QuizMakerAppState } from "../model";
import { categoryReducer, categoryState, ActionType as CategoryActionType } from "./categoryReducer";
import { questionReducer, questionState, ActionType as QuestionActionType } from "./questionReducer";
import { answerReducer, answerState, ActionType as AnswerActionType } from './answerReducer'
import { filterReducer, filterState, ActionType as FilterActionType } from "./filterReducer";

export const reducers = (
  { question, category, answer, filter }: QuizMakerAppState, action: Action<unknown>) => {
  return {
    category: categoryReducer(category, action),
    question: questionReducer(question, action),
    answer: answerReducer(answer, action),
    filter: filterReducer(filter, action)
  }
};

export {
  categoryState, CategoryActionType, questionState, QuestionActionType,
  answerState, AnswerActionType, filterState, FilterActionType
}

export const initialState: QuizMakerAppState = {
  category: categoryState,
  question: questionState,
  answer: answerState,
  filter: filterState
}

export default reducers;
