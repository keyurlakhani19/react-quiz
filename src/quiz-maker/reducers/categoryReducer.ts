import { Action, Category, CategoryState } from '../model'

export const ActionType = {
  Load: '[Category] Load',
  LoadSuccess: '[Category] Load Success',
  LoadFailure: '[Category] Load Failure'
}

export const categoryState: CategoryState = {
  loading: false,
  data: [],
  error: ''
}

export const categoryReducer = (state = categoryState, action: Action<unknown>) => {
  switch (action.type) {
    case ActionType.Load:
      return {
        ...state,
        loading: true
      }

    case ActionType.LoadSuccess:
      return {
        ...state,
        data: action.payload as Category[],
        loading: false
      }

    case ActionType.LoadFailure:
      return {
        ...state,
        data: [],
        loading: false,
        error: String(action.payload)
      }

    default:
      return state;
  }
}