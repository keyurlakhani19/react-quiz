import { Action, FilterState } from "../model"

export const filterState: FilterState = {
  value: {category: null, difficulty: ''},
  invalid: false
}

export const ActionType = {
  Reset: '[Filter] Reset',
  Submit: '[Filter] Submit'
}

export const filterReducer = (state = filterState, action: Action<unknown>) => {
  switch (action.type) {
    case ActionType.Reset:
      return {
        value: {category: null, difficulty: ''},
        invalid: true
      }

    case ActionType.Submit:
      const { value, invalid } = action.payload as FilterState;
      return {
        value,
        invalid
      }

    default:
      return state;
  }
}