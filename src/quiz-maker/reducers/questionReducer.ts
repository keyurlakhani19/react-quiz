import { Action, QuestionState, Question } from '../model'

export const ActionType = {
  Load: '[Question] Load',
  LoadSuccess: '[Question] Load Success',
  LoadFailure: '[Question] Load Failure',
  Reset: '[Question] Reset'
}

export const questionState: QuestionState = {
  loading: false,
  data: [],
  error: ''
}

export const questionReducer = (state = questionState, action: Action<unknown>) => {
  switch (action.type) {
    case ActionType.Load:
      return {
        ...state,
        loading: true,
        data: [],
        error: ''
      }

    case ActionType.LoadSuccess:
      return {
        ...state,
        data: action.payload as Question[],
        loading: false
      }

    case ActionType.LoadFailure:
      return {
        ...state,
        data: [],
        loading: false,
        error: String(action.payload)
      }

    case ActionType.Reset:
      return {
        data: [],
        loading: false,
        error: ''
      }

    default:
      return state;
  }
}