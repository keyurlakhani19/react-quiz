import React, { useCallback, useMemo, useState } from 'react';
import { Question } from '../model';
import { useAnswerStateContext, useStateDispatchContext } from '../state';
import { selectAnswerAction } from '../action/answerAction';

export const Quiz = ({ question, questionIndex }: { question: Question, questionIndex: number }) => {

  const {data, submitted} = useAnswerStateContext();
  const dispatch = useStateDispatchContext();

  const getBtnClassName = useCallback((ans: string) => {
    const { answer: userAnswer, correctAnswer } = data[questionIndex] ?? {}
    let className = 'primary';
    if (submitted) {
      if(ans === correctAnswer) {
        className = 'tertiary'
      } else if(ans === userAnswer && ans === correctAnswer) {
        className = 'tertiary'
      } else if(ans === userAnswer){
        className = 'secondary'
      }

    } else {
      if(userAnswer === ans) {
        className = 'tertiary'
      }
    }

    return className;

  }, [data, submitted, questionIndex, question]);

  const handleAnswerClick = useCallback((answer: string, answerIndex: number) => {
    dispatch(selectAnswerAction({ answer, answerIndex, questionIndex, correctAnswer: question.correct_answer }))
  }, [dispatch, questionIndex, question])

  return (
    <>
      <p dangerouslySetInnerHTML={{ __html: question.question }} />
      {
        question.all_answers.map((answer, i) => {
          return (
            <button key={`answer-${questionIndex}-${i}`} type='button'
              className={getBtnClassName(answer)}
              onClick={() => submitted ? null : handleAnswerClick(answer, i)}
              dangerouslySetInnerHTML={{ __html: answer }}></button>
          )
        })
      }
    </>
  );
}
