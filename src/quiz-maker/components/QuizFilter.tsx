import React, { useCallback, useEffect, useState } from 'react';
import { useFilterStateContext, useStateDispatchContext } from '../state';
import { CategoryDropdownSelect } from './CategoryDropdownSelect';
import { DifficultyDropdownSelect } from './DifficultyDropdownSelect';
import { loadCategories, submitFilterAction } from '../action';
import { Category, FilterStateValue } from '../model';

export const QuizFilter = ({ onCreate }: { onCreate: (value: FilterStateValue) => void }) => {
  const dispatch = useStateDispatchContext();
  const filterState = useFilterStateContext();

  const [invalid, setInValid] = useState(true)
  const [value, setValue] = useState<FilterStateValue>({ category: null, difficulty: '' });

  useEffect(() => {
    const {value} = filterState;
    setValue(value);
  }, [filterState])

  useEffect(() => {
    const invalid = isInvalid(value);
    console.log('invalid..', invalid);
    setInValid(invalid);
  }, [value])

  const isInvalid = useCallback((value: FilterStateValue) => {
    const invalid = Object.keys(value).some(key => {
      if(key === 'category') {
        return !(value[key] as Category)
      } else if(key === 'difficulty') {
        return !value[key]
      }
      
    });
    return invalid;
  }, [])

  const updateValue = useCallback((key: string, value: Category | string | null) => {
    setValue(prev => {
      return {
        ...prev,
        [key]: value
      }
    })
  }, [])

  useEffect(() => {
    loadCategories(dispatch)
  }, [dispatch])

  const handleCreate = useCallback(() => {
    dispatch(submitFilterAction({value, invalid: false}))
    onCreate(value);
  }, [onCreate, value, dispatch])

  return (
    <div className='row' style={{ alignItems: 'center' }}>
      <div><CategoryDropdownSelect value={value.category as Category} onChange={(value) => updateValue('category', value)} /></div>
      <div><DifficultyDropdownSelect value={value.difficulty} onChange={(value) => updateValue('difficulty', value)} /></div>
      <div>
        <button disabled={invalid} type='button' className='primary' onClick={() => handleCreate()}>Create</button>
      </div>
    </div>
  )
}