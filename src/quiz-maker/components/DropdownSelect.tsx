import React, { ChangeEvent, useCallback } from 'react';
import { SelectOption } from '../model';

export const DropdownSelect = ({value, data, id, onChange}: {value: unknown, data: SelectOption[], id: string, onChange: (value: unknown) => void}) => {

  const handleChange = useCallback((event: ChangeEvent) => {
    const evTarget = event.target as HTMLSelectElement;
    const selectedIndex = evTarget.value;
    const value = selectedIndex !== '' ? data[Number(selectedIndex)]?.value :  null
    onChange(value);
  }, [data, onChange]);

  return (
    <>
      <select id={id} onChange={handleChange} value={data.findIndex(item => item.value === value)}>
      <option value=''>{'Please Select'}</option>
        {
          data.map((item, i) => {
            return (
              <option key={`${id}-${i}`} value={i}>{item.viewValue}</option>
            )
          })
        }
      </select>
    </>
  )
}
