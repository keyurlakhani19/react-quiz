import React, { useCallback, useEffect, useMemo } from 'react';
import {useNavigate} from 'react-router-dom'
import { QuizList } from './QuizList';
import { useAnswerStateContext, useStateDispatchContext } from '../state';
import { resetAnswerAction } from '../action/answerAction';
import { resetQuestionAction } from '../action';

export const QuizResult = () => {
  const {score, submitted} = useAnswerStateContext();
  const dispatch = useStateDispatchContext();
  const navigate = useNavigate()

  useEffect(() => {
    if(!submitted) {
      navigate('../');
    }
  }, [submitted, navigate])

  const handleCreateQuiz = useCallback(() => {
    dispatch(resetAnswerAction());
    dispatch(resetQuestionAction());
    navigate('../')
  }, [dispatch])

  const scoreClassName = useMemo(() => {
    let scoreClassName = '';
    if(score < 2) {
      scoreClassName = 'secondary'
    } else if(score < 4) {
      scoreClassName = 'tertiary yellow'
    } else {
      scoreClassName = 'tertiary'
    }
    return scoreClassName;
  }, [score])

  return (
    <>
      <QuizList />
      <div className={`responsive-margin responsive-padding box-colored align-center ${scoreClassName}`}>You scored {score} out of 5</div>
      <button type='button' className='inverse large' onClick={() => handleCreateQuiz()}>Create a new quiz</button>
    </>
  )
}
