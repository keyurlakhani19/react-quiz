import React from 'react';
import { useQuestionStateContext } from '../state';
import { Quiz } from './Quiz';

export const QuizList = () => {
  const {data, loading} = useQuestionStateContext();

  if(loading) {
    return (<div className="spinner primary"></div>)
  }

  return (
    <>
     {
      data.map((item, i) => {
        return (
          <Quiz key={`question-${i}`} question={item} questionIndex={i}/>
        )
      })
     } 
    </>
  );
}
