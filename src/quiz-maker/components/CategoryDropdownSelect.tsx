import React, { useMemo } from 'react';
import { useCategoryStateContext } from '../state';
import { DropdownSelect } from './DropdownSelect';
import { Category, SelectOption } from '../model';

export const CategoryDropdownSelect = ({onChange, value}: {value: Category, onChange: (value: Category) => void}) => {
  const { data = [] } = useCategoryStateContext();

  const categorySelectOptions: SelectOption[] = useMemo(() => {
    return data.map(value => ({value, viewValue: value.name}))
  }, [data])

  return (<DropdownSelect value={value} id="categorySelect" data={categorySelectOptions} onChange={(value) => onChange(value as Category)}/>)
}
