import React from 'react';
import { DropdownSelect } from './DropdownSelect';
import { SelectOption } from '../model';

const data = ['Easy', 'Medium', 'Hard']

const difficultySelectOptions: SelectOption[] = data.map(value => ({ value, viewValue: value }))

export const DifficultyDropdownSelect = (
  { onChange, value }: { value: string, onChange: (value: string) => void }) => {
  return (
    <DropdownSelect 
      id="difficultySelect" 
      value={value}
      data={difficultySelectOptions}
      onChange={(value) => onChange(value as string)} />
    )
}
