import React, { useCallback, useEffect } from 'react';
import {Link, useNavigate} from 'react-router-dom'

import { fetchQuestions } from '../api';

import { useAnswerStateContext, useStateDispatchContext } from '../state';
import { loadQuestions, loadQuestionsAction, resetQuestionAction } from '../action/questionActions';
import { resetAnswerAction, submitAnswerAction } from '../action/answerAction';
import { resetFilterAction } from '../action/filterAction';

import { QuizFilter } from "./QuizFilter"
import { QuizList } from "./QuizList"
import { Category, FetchQuestionsOptions, FilterStateValue } from '../model';

export const QuizMaker = () => {
  const dispatch = useStateDispatchContext();
  const answerState = useAnswerStateContext();
  const navigate = useNavigate()

  const handleQuizFilterCreate = useCallback(async (value: FilterStateValue) => {
    dispatch(resetAnswerAction())
    dispatch(loadQuestionsAction())
    const { category, difficulty } = value;
    const options: FetchQuestionsOptions = { categoryId: (category as Category).id, difficulty }
    await loadQuestions(dispatch, options);
  }, [dispatch, fetchQuestions])

  const handleSubmit = useCallback(() => {
    dispatch(submitAnswerAction(true))
    dispatch(resetFilterAction())
    navigate('results');
  }, [])

  return (
    <div className='container'>
      <QuizFilter onCreate={handleQuizFilterCreate} />
      <QuizList />
      {answerState.data.length === 5 && (
        <p>
          <button type='button' className='inverse large'
            onClick={() => handleSubmit()}>Submit</button>
        </p>
      )}
      
    </div>
  )
}
