import React from 'react';
import {Routes, Route} from 'react-router-dom'
import { QuizMakerApp } from './QuizMakerApp';
import { QuizMaker } from './components/QuizMaker';
import { QuizResult } from './components/QuizResult';

export const QuizMakerRoutes = () => {
  return (
    <Routes>
      <Route element={<QuizMakerApp />}>
        <Route index element={<QuizMaker />} />
        <Route path='results' element={<QuizResult />} />
      </Route>
    </Routes>
  )
}

export default QuizMakerRoutes;